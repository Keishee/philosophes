/*
** threads.c for threads in /home/da-sil_l/Depos/philo/PSU_2014_philo
** 
** Made by Clement Da Silva
** Login   <da-sil_l@epitech.net>
** 
** Started on  Fri Feb 20 10:18:15 2015 Clement Da Silva
** Last update Fri Feb 27 14:23:12 2015 Alex Dana
*/

#include <stdio.h>
#include <stdlib.h>
#include "philo.h"

int	start_philos(t_philo *philos)
{
  if (create_threads(philos) == ERROR)
    return (ERROR);
  if (join_threads(philos) == ERROR)
    return (ERROR);
  return (SUCCESS);
}

int		join_threads(t_philo *philos)
{
  t_philo	*head;

  head = philos;
  while (philos->next != head)
    {
      if (pthread_join(philos->th, NULL) != 0)
	return (ERROR);
      philos = philos->next;
    }
  if (pthread_join(philos->th, NULL) != 0)
    return (ERROR);
  return (SUCCESS);
}

int		create_threads(t_philo *head)
{
  t_philo	*philos;

  philos = head;
  init_sticks();
  while (philos->next != head)
    {
      if (pthread_create(&philos->th, NULL, &do_philos, philos) != 0)
	return (ERROR);
      philos = philos->next;
    }
  if (pthread_create(&philos->th, NULL, &do_philos, philos) != 0)
    return (ERROR);
  return (SUCCESS);
}

void	xmutex_unlock(pthread_mutex_t *m)
{
  if ((pthread_mutex_unlock(m)) != 0)
    {
      printf("pthread_mutex_unlock failed\n");
      exit(ERROR);
    }
}

void	xmutex_lock(pthread_mutex_t *m)
{
  if ((pthread_mutex_lock(m)) != 0)
    {
      printf("pthread_mutex_lock failed\n");
      exit(ERROR);
    }
}
