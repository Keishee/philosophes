/*
** list.c for kdfl7 in /home/da-sil_l/Depos/philo/PSU_2014_philo
** 
** Made by Clement Da Silva
** Login   <da-sil_l@epitech.net>
** 
** Started on  Thu Feb 19 20:43:42 2015 Clement Da Silva
** Last update Fri Feb 27 14:58:41 2015 Alex Dana
*/

#include <stdlib.h>
#include <stdio.h>
#include "philo.h"

char	g_sticks[7][2];

void	sticks_interaction(int philo, int flags, int id)
{
  while (flags > 0)
    {
      g_sticks[philo][0] = flags % 10;
      g_sticks[philo][1] = id;
      philo = (philo + 1) % 7;
      flags /= 10;
    }
  xmutex_unlock(&g_m);
}

void	init_sticks()
{
  int	i;

  i = 0;
  while (i < 7)
    {
      g_sticks[i][0] = FREE;
      g_sticks[i][1] = -1;
      ++i;
    }
}

t_philo		*init_list()
{
  t_philo	*head;

  if ((head = malloc(sizeof(*head))) == NULL)
    return (NULL);
  head->id = 0;
  head->state = SLEEPING;
  head->next = head;
  head->prev = head;
  return (head);
}

t_philo		*fill_list(t_philo *head)
{
  int		i;
  t_philo	*new_elem;

  i = 1;
  while (i < 7)
    {
      if ((new_elem = malloc(sizeof(*new_elem))) == NULL)
	return (NULL);
      new_elem->id = i;
      new_elem->rightID = (i + 1) % 7;
      new_elem->state = SLEEPING;
      new_elem->next = head;
      head->prev->next = new_elem;
      new_elem->prev = head->prev;
      head->prev = new_elem;
      ++i;
    }
  return (head);
}
