/*
** philo.c for philo in /home/da-sil_l/Depos/philo/PSU_2014_philo
** 
** Made by Clement Da Silva
** Login   <da-sil_l@epitech.net>
** 
** Started on  Mon Feb 23 20:24:15 2015 Clement Da Silva
** Last update Fri Feb 27 14:59:41 2015 Alex Dana
*/

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/times.h>
#include "philo.h"

char		g_sticks[7][2];
pthread_mutex_t	g_m = PTHREAD_MUTEX_INITIALIZER;

void		do_eat(t_philo *philo)
{
  sticks_interaction(philo->id, TAKEN_TAKEN, philo->id);
  philo->state = EATING;
  printf("\033[31m%d IS EATING\033[00m\n", philo->id);
  sleep(EAT_TIME);
  xmutex_lock(&g_m);
  sticks_interaction(philo->id, FREE_FREE, -1);
  printf("\033[31m%d STOPS EATING\033[00m\n", philo->id);
}





void		do_think(t_philo *philo, int id)
{
  int		id_to_take;
  time_t	ctime;

  sticks_interaction(id, TAKEN, philo->id);
  philo->state = THINKING;
  sleep(THINK_TIME);
  ctime = time(NULL);
  printf("\033[36m%d IS THINKING\033[00m\n", philo->id);
  xmutex_lock(&g_m);
  sticks_interaction(id, FREE, -1);
  id_to_take = (id == philo->id ? philo->next->id : philo->id);
  while (philo->state == THINKING)
    {
      if (time(NULL) - ctime > 15)
	exit(3);
      usleep(30);
      xmutex_lock(&g_m);
      if (g_sticks[id_to_take][0] == FREE && g_sticks[id][0] == FREE)
	do_eat(philo);
      else
	xmutex_unlock(&g_m);
    }
  xmutex_lock(&g_m);
  do_sleep(philo);
}

void		do_sleep(t_philo *philo)
{
  xmutex_unlock(&g_m);
  printf("\033[35m%d IS SLEEPING\033[00m\n", philo->id);
  sleep(SLEEP_TIME);
  while (philo->state == SLEEPING)
    {
      usleep(50);
      xmutex_lock(&g_m);
      if (g_sticks[philo->next->id][0] == FREE &&
	  g_sticks[philo->id][0] == FREE)
	do_eat(philo);
      else if (g_sticks[philo->id][0] == FREE ||
	       g_sticks[philo->next->id][0] == FREE)
	{
	  if (g_sticks[philo->id][0] == FREE)
	    do_think(philo, philo->id);
	  else
	    do_think(philo, philo->next->id);
	}
      else
	xmutex_unlock(&g_m);
    }
}

void		*do_philos(void *arg)
{
  t_philo	*philo;

  philo = (t_philo *)arg;
  while (1)
    {
      xmutex_lock(&g_m);
      if (philo->state != EATING &&
	  g_sticks[philo->id][0] == FREE &&
	  g_sticks[philo->next->id][0] == FREE)
	do_eat(philo);
      else if (g_sticks[philo->id][0] == FREE ||
	       g_sticks[philo->next->id][0] == FREE)
	{
	  if (g_sticks[philo->id][0] == FREE)
	    do_think(philo, philo->id);
	  else
	    do_think(philo, philo->next->id);
	}
      else
	do_sleep(philo);
    }
  printf("%d has left the table\n", philo->id);
  return (NULL);
}
