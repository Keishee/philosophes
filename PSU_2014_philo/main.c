/*
** main.c for main in /home/dana_a/rendu/philosophes/PSU_2014_philo
** 
** Made by Alex Dana
** Login   <dana_a@epitech.net>
** 
** Started on  Fri Feb 27 14:52:32 2015 Alex Dana
** Last update Fri Feb 27 14:53:22 2015 Alex Dana
*/

#include "philo.h"

int		main()
{
  t_philo	*philos;

  init_sticks();
  if ((philos = init_list()) == NULL)
    return (ERROR);
  if ((philos = fill_list(philos)) == NULL)
    return (ERROR);
  start_philos(philos);
  return (SUCCESS);
}
