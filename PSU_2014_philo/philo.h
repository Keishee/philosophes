/*
** philo.h for philo in /home/da-sil_l/rendu/PSU_2014_philo
** 
** Made by Clement Da Silva
** Login   <da-sil_l@epitech.net>
** 
** Started on  Thu Feb 19 14:46:11 2015 Clement Da Silva
** Last update Fri Feb 27 14:36:27 2015 Alex Dana
*/

#ifndef PHILO_H_
# define PHILO_H_

# include <pthread.h>

# define SLEEPING	0
# define EATING		1
# define THINKING	2

# define TAKEN		3
# define FREE		4
# define FREE_FREE	44
# define TAKEN_TAKEN	33
# define TAKEN_FREE	34
# define FREE_TAKEN	43

# define ERROR		5
# define SUCCESS	6

# define SLEEP_TIME	2
# define EAT_TIME	1
# define THINK_TIME	1

extern pthread_mutex_t	g_m;
extern char		g_sticks[7][2];

typedef struct		s_philo
{
  struct s_philo	*next;
  struct s_philo	*prev;
  pthread_t		th;
  int			id;
  int			rightID;
  int			state;
}			t_philo;

t_philo		*init_list();
t_philo		*fill_list(t_philo *head);
void		*do_philos(void *);
void		init_sticks();
void		print_sticks();
void		sticks_interaction(int philo, int flags, int id);
void		xmutex_unlock(pthread_mutex_t *m);
void		xmutex_lock(pthread_mutex_t *m);
void		do_eat(t_philo *philo);
void		do_sleep(t_philo *philo);
void		do_think(t_philo *philo, int id);
int		start_philos(t_philo *philo);
int		join_threads(t_philo *philo);
int		create_threads(t_philo *philo);

#endif /* !PHILO_H_ */
